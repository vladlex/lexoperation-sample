//
//  LEXImagePackEntry.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 16.07.14.
//
//

#import "LEXImagePackEntry.h"

@implementation LEXImagePackEntry

+ (instancetype)entryWithLink:(NSString *)link
{
    return [[self alloc] initWithSourceURL:[NSURL URLWithString:link]];
}

- (id)init
{
    return [self initWithSourceURL:nil];
}

- (id)initWithSourceURL:(NSURL *)URL
{
    self = [super init];
    if (self != nil) {
        _sourceURL = URL;
        
    }
    return self;
}

- (BOOL)isLoadingFinished
{
    return (_state == kLEXImagePackEntryStateLoaded ||
            _state == kLEXImagePackEntryStateFailed);
}

- (void)configureWithFailError:(NSError *)error
{
    _error = error;
    _image = nil;
    self.state = kLEXImagePackEntryStateFailed;
}

- (void)configureWithLoadedImage:(UIImage *)image
{
    _image = image;
    _error = nil;
    self.state = kLEXImagePackEntryStateLoaded;
}

@end
