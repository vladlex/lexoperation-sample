//
//  LEXImageResizeOperation.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 17.07.14.
//
//

#import "LEXImageResizeOperation.h"

@interface LEXImageResizeOperation()

@end

@implementation LEXImageResizeOperation

- (id)init
{
    return [self initWithImage:nil
                    targetSize:CGSizeZero];
}

- (id)initWithImage:(UIImage *)image
          sideLimit:(CGFloat)sideLimit
{
    CGFloat scale;
    if (image.size.width == 0.0 ||
        image.size.height == 0.0) {
        scale = 0.0f;
    }
    else {
        scale = MIN(sideLimit / image.size.width,
                    sideLimit / image.size.height);
    }
    return [self initWithImage:image
                         scale:scale];
}

- (id)initWithImage:(UIImage *)image
              scale:(CGFloat)scale
{
    CGSize size = CGSizeMake(image.size.width * scale,
                             image.size.height * scale);
    return [self initWithImage:image
                    targetSize:size];
}

- (id)initWithImage:(UIImage *)image
         targetSize:(CGSize)size
{
    self = [super init];
    if (self != nil) {
        _image = image;
        _targetSize = size;
    }
    return self;
}

- (void)operationDidStart
{
    [super operationDidStart];
    
    UIImage *image = [self resizedImage];
    [self finishWithSuccessAndOutputData:image];
}

/** All using UIGraphics methods are thread-safe since iOS 4 */
- (UIImage *)resizedImage
{
    CGSize size = self.targetSize;
    UIGraphicsBeginImageContext(size);
    CGRect imageBounds = CGRectMake(0.0f, 0.0f, size.width, size.height);
    [self.image drawInRect:imageBounds];
    UIImage *resultImage = UIGraphicsGetImageFromCurrentImageContext();
    return resultImage;
}

@end
