//
//  LEXOperationResult.m
//
//  Created by Aleksei Gordeev on 20.01.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import "LEXOperationResult.h"

@interface LEXOperationResult ()

@end

@implementation LEXOperationResult

+ (instancetype)resultFailWithError:(NSError *)error
{
    LEXOperationResult *result;
    result = [[self alloc] initWithSuccess:NO
                                     error:error
                                outputData:nil];
    return result;
}

+ (instancetype)resultSuccessWithOutputData:(id)data
{
    LEXOperationResult *result;
    result = [[self alloc] initWithSuccess:YES
                                     error:nil
                                outputData:data];
    return result;
}

+ (instancetype)resultCancel
{
    NSError *cancelError = [NSError errorWithDomain:NSCocoaErrorDomain
                                               code:NSUserCancelledError
                                           userInfo:nil];
    return [[self alloc] initWithSuccess:NO
                                   error:cancelError
                              outputData:nil];
}

- (id)init
{
    return [self initWithSuccess:NO
                           error:nil
                      outputData:nil];
}

- (id)initWithSuccess:(BOOL)success
                error:(NSError *)error
           outputData:(id)data
{
    self = [super init];
    if (self != nil) {
        _success = success;
        _error = error;
        [self setOutputData: data];
    }
    return self;
}

- (void)setOutputData:(id)data
{
    _outputData = data;
}

- (id)outputData
{
    return _outputData;
}

- (NSUInteger)hash
{
    return (_success ? 1 : 0) + _error.code;
}

- (BOOL)isEqual:(id)object
{
    if (self == object) {
        return YES;
    }
    if ([object isKindOfClass:self.class]) {
        LEXOperationResult *anotherResult = object;
        if (self.success == anotherResult.success &&
            [self.error isEqual:anotherResult.error] &&
            [self.outputData isEqual:anotherResult.outputData]) {
            return YES;
        }
    }
    return NO;
}

- (BOOL)isCancelled
{
    if (_error) {
        if ([_error.domain isEqualToString:NSCocoaErrorDomain] &&
            _error.code == NSUserCancelledError) {
            return YES;
        }
    }
    return NO;
}

- (NSString *)description
{
    NSString *status;
    if (self.success) {
        status = @"Success";
    }
    else if (self.isCancelled){
        status = @"Cancelled";
    }
    else {
        status = [NSString stringWithFormat:@"Failed: %@", self.error];
    }
    return [NSString stringWithFormat:@"<%@: %p, %@>",
            NSStringFromClass(self.class),
                              self,
                              status];
}

@end


