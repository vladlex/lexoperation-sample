//
//  LEXOperationResult.h
//
//  Created by Aleksei Gordeev on 20.01.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import <Foundation/Foundation.h>

/** This is an abstract class.
 You should override 'setOutputData' and 'outputData' methods.
 The best way to do it is just create appropriate strong/copy property.
 Also, it's good practise to create additional getter to outputData 
    with understandable name and appropriate returning object's class.
 */
@interface LEXOperationResult : NSObject
{
    @protected
    id _outputData;
}

/** Summary operation completion state */
@property (nonatomic, assign, readonly) BOOL success;
/** Operation failure reason error */
@property (nonatomic, strong, readonly) NSError *error;
/** Any additional info. Available for changes at any time.*/
@property (nonatomic, strong) NSDictionary *userInfo;

/** Create and returns initialized LEXOperationResult object with 'success' mark and given data
 @param data Operation completion results.
 @return An initialized LEXOperationResult or it's subclass object. */
+ (instancetype)resultSuccessWithOutputData:(id)data;

/** Create and returns initialized LEXOperationResult object with 'fail' mark and given error
 @param data Operation completion results.
 @return An initialized LEXOperationResult or it's subclass object. */
+ (instancetype)resultFailWithError:(NSError *)error;

/** Create and returns initialized LEXOperationResult with 'fail' mark and 'cancel' error.
 'Cancel' error is error with NSCocoaErrorDomain domain and NSUserCancelledError error code.
 @param data Operation completion results.
 @return An initialized LEXOperationResult or it's subclass object.
 */
+ (instancetype)resultCancel;

/** Create and returns initialized LEXOperationResult object with given data. Designated initializer.
 @param success Results completion state.
 @param error Results error (if results is unsuccessfull)
 @param data Results completion data.
 @return An initialized LEXOperationResult or it's subclass object.
 */
- (id)initWithSuccess:(BOOL)success
                error:(NSError *)error
           outputData:(id)data;

/** Default setter for result data. Usually you declare special subclass property with needed class and linking option (strong/copy). */
- (void)setOutputData:(id)data;
/** Default getter for result data. Usually you declare handy accessor with needed class. */
- (id)outputData;

/** Return YES if operation was cancelled. 
 The operation considered cancelled if error has domain NSCocoaErrorDomain 
    and error code is NSUserCancelledError*/
- (BOOL)isCancelled;

@end


