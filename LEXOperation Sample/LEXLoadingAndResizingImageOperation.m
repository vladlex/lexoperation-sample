//
//  LEXLoadingAndResizingImageOperation.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 17.07.14.
//
//

#import "LEXLoadingAndResizingImageOperation.h"

#import "LEXURLConnectionDataLoadOperation.h"
#import "LEXImageResizeOperation.h"

#import "LEXOperationMacros.h"

@implementation LEXLoadingAndResizingImageOperation

- (id)init
{
    return [self initWithImageURL:nil
                   imageLimitSide:0.0];
}

- (id)initWithImageURL:(NSURL *)URL
        imageLimitSide:(CGFloat)limitSide
{
    self = [super init];
    if (self != nil) {
        _URL = URL;
        _imageLimitSide = limitSide;
    }
    return self;
}

- (void)operationDidStart
{
    [super operationDidStart];
    [self loadImage];
}

- (void)loadImage
{
    LEXURLConnectionDataLoadOperation *op;
    op = [[LEXURLConnectionDataLoadOperation alloc] initWithURL:_URL];
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
        LEXURLConnectionDataLoadOperationResult *result = (id)r;
        if (!result.success) {
            [safeSelf finishWithFailAndError:result.error];
        }
        else {
            [safeSelf resizeImageWithData:result.data];
        }
    }];
    [self addSuboperation:op];
}

- (void)resizeImageWithData:(NSData *)data
{
    if (self.isInDebugMode) {
        NSLog(@"Recieve data: %@", @(data.length));
    }
    LEXOperationStopIfCancelled;
    
    UIImage *image = [UIImage imageWithData:data];
    LEXImageResizeOperation *op;
    __weak typeof(self) safeSelf = self;
    op = [[LEXImageResizeOperation alloc] initWithImage:image
                                              sideLimit:self.imageLimitSide];
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
        LEXOperationResult *result = r;
        [safeSelf finishWithSuccessAndOutputData:result.outputData];
    }];
    [self addSuboperation:op];
}

@end
