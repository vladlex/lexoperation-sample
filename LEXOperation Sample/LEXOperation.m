//
//  LEXOperation.m
//
//  Created by Aleksei Gordeev on 20.01.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

/** LEXOperationCheckCurrentQueueIsActual macro
 LEXOperationCheckCurrentQueueIsActual is using for checking if current queue is actual queue.
 For using this macro your class should have actualDispatchQueue.
 This macro using dispatch_get_current_queue deprecated method IN DEBUG MODE ONLY.
 Please, do not worry about production.
 Also you can turn off LEXOperationDebug mode in LEXOperationMacros.h
 */

#import "LEXOperation.h"

#import "LEXOperationMacros.h"

@interface LEXOperation ()
@property (nonatomic, copy) LEXOperationCompletionBlock queuedCompletionBlock;
@property (atomic, copy) LEXOperationProgressHandlerBlock queuedProgressHandlerBlock;

@property (atomic, strong, readwrite) dispatch_queue_t actualDispatchQueue;
@property (atomic, strong, readwrite) dispatch_queue_t actualCallbacksDispatchQueue;



@property (nonatomic, strong, readwrite) LEXOperationResult *result;
@property (nonatomic, assign, readwrite) LEXOperationState   state;

- (void)startWithCorrectQueue;
- (void)cancelWithCorrectQueue;

- (void)performThreadedCompletionBlockIfNeeded;
- (void)finishWithResultOnDispatchQueue:(LEXOperationResult *)result;

@end

@implementation LEXOperation

- (id)init
{
    self = [super init];
    if (self) {
        _state = kLEXOperationStateInited;
        _dispatchQueue = nil;
        _actualDispatchQueue = nil;
        _callbacksDispatchQueue = nil;
    }
    return self;
}

- (void)dealloc
{
    if (_state == kLEXOperationStateExecuting) {
        [self cancel];
    }
}

- (void)finishWithResult:(LEXOperationResult *)result
{
    dispatch_async(self.actualDispatchQueue, ^{
        [self finishWithResultOnDispatchQueue:result];
    });
}

- (void)finishWithResultOnDispatchQueue:(LEXOperationResult *)result
{
    LEXOperationCheckCurrentQueueIsActual;
    if (self.isFinished) {
        return;
    }
    self.result = result;
    [self operationWillFinish];
    self.state = kLEXOperationStateFinished;
    [self performThreadedCompletionBlockIfNeeded];
    [self operationDidFinish];
    [self cleanUp];
}

-  (void)setState:(LEXOperationState)state
{
    if (_state == state) {
        return;
    }
    assert(state != kLEXOperationStateInited);
    
    LEXOperationState oldState;
    oldState = _state;
    if ( (state == kLEXOperationStateExecuting) ||
        (oldState == kLEXOperationStateExecuting) ) {
        [self willChangeValueForKey:@"isExecuting"];
    }
    if (state == kLEXOperationStateFinished) {
        [self willChangeValueForKey:@"isFinished"];
    }
    _state = state;
    if (state == kLEXOperationStateFinished) {
        [self didChangeValueForKey:@"isFinished"];
    }
    if ( (state == kLEXOperationStateExecuting) ||
        (oldState == kLEXOperationStateExecuting) ) {
        [self didChangeValueForKey:@"isExecuting"];
    }
}

- (void)startWithCorrectQueue
{
    LEXOperationCheckCurrentQueueIsActual;
    if (self.state == kLEXOperationStateFinished) {
        return;
    }
    if (self.isCancelled) {
        [self finishWithCancelAndUserInfo:nil];
    }
    else {
        [self operationDidStart];
    }
}

- (void)cancelWithCorrectQueue
{
    LEXOperationCheckCurrentQueueIsActual;
    if (self.state == kLEXOperationStateExecuting) {
        [self finishWithCancelAndUserInfo:nil];
    }
}

- (void)start
{
    if (self.state == kLEXOperationStateInited) {
        self.state = kLEXOperationStateExecuting;
        dispatch_queue_t queue = self.dispatchQueue;
        if (!queue) {
            dispatch_queue_priority_t priority =  DISPATCH_QUEUE_PRIORITY_HIGH;
            queue = dispatch_get_global_queue(priority,
                                              0);
        }
        self.actualDispatchQueue = queue;
        dispatch_queue_t callbacksQueue = self.callbacksDispatchQueue;
        if (!callbacksQueue) {
            callbacksQueue = dispatch_get_main_queue();
        }
        self.actualCallbacksDispatchQueue = callbacksQueue;
        dispatch_sync(self.actualDispatchQueue, ^{
            [self startWithCorrectQueue];
        });
    }
}

- (void)cancel
{
    if (self.isExecuting && !self.isCancelled) {
        dispatch_async(self.actualDispatchQueue, ^{
            [self cancelWithCorrectQueue];
        });
    }
}

- (BOOL)isExecuting
{
    return self.state == kLEXOperationStateExecuting;
}

- (BOOL)isFinished
{
    return self.state == kLEXOperationStateFinished;
}

- (BOOL)isConfigurable
{
    return self.state == kLEXOperationStateInited;
}

- (BOOL)isConcurrent
{
    return YES;
}

- (void)performThreadedCompletionBlockIfNeeded
{
    LEXOperationCompletionBlock block = _queuedCompletionBlock;
    if (block) {
        LEXOperationResult *result = self.result;
        dispatch_queue_t queue = self.actualCallbacksDispatchQueue;
        if (!queue) {
            queue = self.actualDispatchQueue;
        }
        dispatch_async(queue, ^{
            block(result);
        });
    }
}

- (BOOL)isInDebugMode
{
#ifdef DEBUG
    return self.debugMode;
#else
    return NO;
#endif
}

@end

@implementation LEXOperation (Subclassing)

+ (Class)resultClass
{
    return [LEXOperationResult class];
}

- (void)declareProgress:(float)progress
               userInfo:(id)userInfo
{
    LEXOperationProgressHandlerBlock block = self.queuedProgressHandlerBlock;
    if (block) {
        dispatch_queue_t queue = self.actualCallbacksDispatchQueue;
        dispatch_async(queue, ^{
            block(progress, userInfo);
        });
    }
}

- (BOOL)isProgressable
{
    return NO;
}

- (void)operationDidStart
{
    dispatch_queue_t dispatchQueue = self.dispatchQueue;
    if (!dispatchQueue) {
        dispatchQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,
                                                  0);
    }
    self.actualDispatchQueue = dispatchQueue;
    
    dispatch_queue_t callbacksDispatchQueue = self.callbacksDispatchQueue;
    if (!callbacksDispatchQueue) {
        callbacksDispatchQueue = dispatch_get_main_queue();
    }
    self.actualCallbacksDispatchQueue = callbacksDispatchQueue;
}

- (void)operationWillFinish
{
    // Do nothing
}

- (void)operationDidFinish
{
    // Do nothing
}

- (void)cleanUp
{
    // Do nothing
}

- (void)operationWillCancel
{
    // Do nothing
}

- (void)finishWithFailAndError:(NSError *)error
{
    NSError *localizedError = [self localizedErrorWithError:error];
    if (!localizedError) {
        localizedError = error;
    }
    LEXOperationResult *result;
    result = [[self.class resultClass] resultFailWithError:localizedError];
    [self finishWithResult:result];
}

- (void)finishWithSuccessAndOutputData:(id)data
{
    LEXOperationResult *result;
    result = [[self.class resultClass] resultSuccessWithOutputData:data];
    [self finishWithResult:result];
}

- (void)finishWithCancelAndUserInfo:(NSDictionary *)userInfo
{
    LEXOperationResult *result;
    result = [[self.class resultClass] resultCancel];
    result.userInfo = userInfo;
    [self finishWithResult:result];
}

- (float)progress
{
    if (self.isExecuting) {
        return 0.5f;
    }
    return self.isFinished ? 1.0f : 0.0f;
}

- (NSString *)description
{
    NSString *main = [NSString stringWithFormat:@"%@: %p",
                      NSStringFromClass(self.class), self];
    NSArray *info = @[main];
    if (self.title) {
        NSString *title = [NSString stringWithFormat:@"title: %@",
                           self.title];
        info = [info arrayByAddingObject:title];
    }
    return [NSString stringWithFormat:@"<%@>",
            [info componentsJoinedByString: @", "]];
}

- (NSError *)localizedErrorWithError:(NSError *)sourceError
{
    return nil;
}

@end
