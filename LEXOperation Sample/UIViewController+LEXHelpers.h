//
//  UIViewController+LEXHelpers.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (LEXHelpers)

/** This method solves invalid view layout under navigation bar. 
 This method safe to call in any iOS
 */
- (void)prepareForExtendedLayoutWithNoEdges;

@end
