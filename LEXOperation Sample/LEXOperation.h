//
//  LEXOperation.h
//
//  Created by Aleksei Gordeev on 20.01.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//
//  /// /// /// ///
//
//  This class is inspired by QRunLoopOperation
//      from LinkedImageFetcher Apple Sample project.
//
/*  https://developer.apple.com/library/mac/samplecode/LinkedImageFetcher/Introduction/Intro.html
    (You should be registered apple developer)
 */
//
//  /// /// /// ///

#import <Foundation/Foundation.h>

#import "LEXOperationResult.h"

#pragma mark General

/*
 For sublcassing see 'Subclassing' category below.
 */

typedef NS_ENUM(int, LEXOperationState)
{
    kLEXOperationStateInited = 0,       ///< Operation is configurable.
    kLEXOperationStateExecuting,        ///< Operation is executing.
    kLEXOperationStateFinished          ///< Operation finished. See 'result' property.
};

/** LEXOperationCompletionBlock operation completion block with summary result.
 @warning Pay attention to __unsafe_unretained result ownership qualifier. If you want to use result in any way you should create strong variable in completion block:
 @code
 [operation setQueuedCompletionBlock:^(__unsafe_unretained LEXOperationResult *result) {
 LEXOperationResult *strongResult = result;
 // Do something with strong result, it's automatically retained now.
 }];
 @endcode
 */
typedef void(^LEXOperationCompletionBlock)(__unsafe_unretained LEXOperationResult *result);

/** LEXOperationProgressHandlerBlock operation progress change notification block/
 */
typedef void(^LEXOperationProgressHandlerBlock)(float progress, __unsafe_unretained id userInfo);

/*  
 For subclassing pay attention to 'Subclassing' Category below
 */
/**
 Completely thread-safe concurrent NSOperation abstract class. For subclassing see 'Subclassing' category.
 */
@interface LEXOperation : NSOperation

@property (atomic, strong) dispatch_queue_t dispatchQueue;
@property (atomic, strong, readonly) dispatch_queue_t actualDispatchQueue;

/** This is queue for performing progress handling and completion blocks, main thread by default. */
@property (atomic, strong) dispatch_queue_t callbacksDispatchQueue;
@property (atomic, strong, readonly) dispatch_queue_t actualCallbacksDispatchQueue;

/** Use 'isInDebugMode' method to check if operation in debug mode.
 isInDebugMode always return 'NO' in production and does not use atomic property at all.
 */
@property (atomic, assign) BOOL debugMode;
- (BOOL)isInDebugMode;

// Useful getters
/** Current operation state.*/
@property (nonatomic, assign, readonly) LEXOperationState   state;
/** Operation result. Empty untill operation is complete. Usually result is subclassing so every operation have an unique result class. */
@property (atomic, strong, readonly) LEXOperationResult *result;

@property (atomic, copy) NSString *title;

/** Finish point for any operation execution even it's failed, cancelled or succesfully completed. Saves the result and declare it's finished. */
- (void)finishWithResult:(LEXOperationResult *)result;

/** Returns 'YES' if state is 'Inited' (operation is never executing yet and may be configured).
    Returns 'NO' in other cases. */
- (BOOL)isConfigurable;

/** Sets completion block which perform on valid queue when operation will end.
 @param queuedCompletionBlock Completion block to perform.
 @warning This completion block pass unsafe results.
        Don't forget to retain it if you want to do something with results.
 @code
 [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
    LEXOperationResult *result = (id)r;
 }
 @endcode
 */
- (void)setQueuedCompletionBlock:(LEXOperationCompletionBlock)queuedCompletionBlock;
/** Returns current completion block which will be performed on needed queue when operation ends. */
- (LEXOperationCompletionBlock)queuedCompletionBlock;

- (void)setQueuedProgressHandlerBlock:(LEXOperationProgressHandlerBlock)block;
- (LEXOperationProgressHandlerBlock)queuedProgressHandlerBlock;

@end

#pragma mark Subclassing And Subclasses frequently using methods

/* --- === For subclassing pay attention to this subcategory === --- */

/** This category will be usefull for subclassing */
@interface LEXOperation (Subclassing)

/** What class of results should be using. */
+ (Class)resultClass;

/** Perform progress handle block on actual callbacks queue */
- (void)declareProgress:(float)progress userInfo:(id)userInfo;

/** Fast method to finish operation with 'fail' mark and explanation error.
 @param error An NSError object describing failure reason.
 @warning If you override this method you should always call finishWithResult: */
- (void)finishWithFailAndError:(NSError *)error;

/** Fast method to finish operation with 'success' mark and results data.
 @param data Operation completion results. The data class is depends on the LEXOperationResult subclass.
 @warning If you override this method you should always call finishWithResult: */
- (void)finishWithSuccessAndOutputData:(id)data;

/** Fast method to finish operation with 'success' mark and results data.
 @param userInfo An NSDictionary object with additional information (not necessary about cancelling).
 @warning If you override this method you should always call finishWithResult:
 */
- (void)finishWithCancelAndUserInfo:(NSDictionary *)userInfo;

/** A place for operation execution starts. It's always run on actual thread in actual mode. All KVO routine already done.
 */
- (void)operationDidStart;

/** Call whe operation is about to finish. The result is already set, but operations state is still 'executing'. it's always run on actual thread.*/
- (void)operationWillFinish;

/** Operation completion method. This is a good place to call your custom completion blocks or delegate methods. */
- (void)operationDidFinish;

/** Override this method to do some work like stop being delegate, cancel suboperations or other stuff. */
- (void)operationWillCancel;

/** Free resources which is not needed after operation completed (but not results!) in ayn way (failel, cancelled, successfully finished)*/
- (void)cleanUp;

/** Returns 'YES' if operation supports progress block perform */
- (BOOL)isProgressable;

- (NSError *)localizedErrorWithError:(NSError *)sourceError;

@end