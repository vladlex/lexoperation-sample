//
//  UIDevice+LEXSystemVersion.m
//
//  Created by Aleksei Gordeev on 21.02.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import "UIDevice+LEXSystemVersion.h"


NSString * const kLEXEqualOrHigherSuffix = @"EH";

@implementation UIDevice (LEXSystemVersion)

static inline NSUInteger LEXGetVersionComponentAtIndex(NSArray *components,
                                                       NSUInteger idx,
                                                       NSNumberFormatter *nf)
{
    NSString *component = components[idx];
    NSNumber *versionNumber = [nf numberFromString:component];
    return [versionNumber unsignedIntegerValue];
}

+ (NSMutableDictionary *)sharedVerCompareInfo {
    __strong static NSMutableDictionary *vcInfo = nil;
    static dispatch_once_t predicate = 0;
    dispatch_once(&predicate, ^{
        vcInfo = [[NSMutableDictionary alloc] init];
    });
    return vcInfo;
}

+ (BOOL)vcInfoHasValueForKey:(NSString *)key
{
    return [[UIDevice sharedVerCompareInfo] objectForKey:key] != nil;
}

+ (NSArray *)systemVersionComponentsFromSystemVersion:(NSString *)version
{
    NSArray *components = [version componentsSeparatedByString:@"."];
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    NSUInteger count = components.count;
    NSMutableArray *vNumComponents = [NSMutableArray arrayWithCapacity:count];
    for (NSString *vComponent in components) {
        NSNumber *num = [formatter numberFromString:vComponent];
        if (num) {
            [vNumComponents addObject:num];
        }
    }
    return vNumComponents.copy;
}

+ (NSArray *)systemVersionComponents
{
    __strong static id sharedObject = nil;
    static dispatch_once_t predicate = 0;
    dispatch_once(&predicate, ^{
        NSString *version = [[self currentDevice] systemVersion];
        sharedObject = [self systemVersionComponentsFromSystemVersion:version];
    });
    return sharedObject;
}

+ (NSComparisonResult)compareCurrentSystemVersionWithVersion:(NSString *)version
{
    NSString *currentVersion = [[self currentDevice] systemVersion];
    return [self compareSystemVersion:currentVersion
                            toVersion:version];
}

+ (NSComparisonResult)compareSystemVersion:(NSString *)version1 toVersion:(NSString *)version2
{
    return [self compareSystemVersion:version1
                            toVersion:version2
                        getDifference:NULL
                                index:NULL];
}

+ (NSComparisonResult)compareSystemVersion:(NSString *)version1
                                 toVersion:(NSString *)version2
                             getDifference:(out NSUInteger *)difference
                                     index:(out NSUInteger *)index
{
    NSComparisonResult result = NSOrderedSame;
    NSUInteger idx;
    NSArray *v1Components, *v2Components;
    v1Components = [self systemVersionComponentsFromSystemVersion:version1];
    v2Components = [self systemVersionComponentsFromSystemVersion:version2];
    NSUInteger v1Count, v2Count;
    v1Count = v1Components.count;
    v2Count = v2Components.count;
    NSUInteger count = MAX(v1Count, v2Count);
    NSUInteger comp1, comp2;
    NSInteger diff;
    for (idx = 0; idx < count; idx ++) {
        comp1 = (idx < v1Count) ? [v1Components[idx] unsignedIntegerValue] : 0;
        comp2 = (idx < v2Count) ? [v2Components[idx] unsignedIntegerValue] : 0;
        diff = comp1 - comp2;
        if (diff != 0) {
            result = (diff > 0) ? NSOrderedDescending : NSOrderedAscending;
            if (difference != NULL) {
                *difference = diff;
            }
            if (index != NULL) {
                *index = idx;
            }
            break;
        }
    }
    return result;
}

- (BOOL)isSystemVersionEqualOrHigher:(NSString *)reqSysVer
{
    
    NSString *key = [reqSysVer stringByAppendingString:kLEXEqualOrHigherSuffix];
    if ([self.class vcInfoHasValueForKey:key]) {
        return [[[self.class sharedVerCompareInfo] objectForKey:key] boolValue];
    }
    NSComparisonResult comparisonResult;
    comparisonResult = [self.class compareSystemVersion:self.systemVersion
                                              toVersion:reqSysVer
                                          getDifference:NULL
                                                  index:NULL];
    BOOL isEorH = (comparisonResult != NSOrderedAscending);
    [self.class sharedVerCompareInfo][key] = [NSNumber numberWithBool:isEorH];
    return isEorH;
}

- (void)getMajorVersion:(out NSUInteger *)majorVersion
           minorVersion:(out NSUInteger *)minorVersion
{
    if (majorVersion == NULL && minorVersion == NULL) {
        return;
    }
    // Get current iOS version.
    NSString *osVersionStr = [self systemVersion];
    /** Version Components. */
    NSArray *vComps = [osVersionStr componentsSeparatedByString:@"."];
    if (vComps.count > 1) {
        NSNumberFormatter *nf = [[NSNumberFormatter alloc] init];
        if (majorVersion ) {
            *majorVersion = LEXGetVersionComponentAtIndex(vComps,
                                                          0,
                                                          nf);
        }
        if (minorVersion) {
            *minorVersion = LEXGetVersionComponentAtIndex(vComps,
                                                          1,
                                                          nf);
        }
    }
}

@end

BOOL LEXCurrentSystemVersionIsEqualOrHigher(NSString *reqSysVer)
{
    UIDevice *device = [UIDevice currentDevice];
    return [device isSystemVersionEqualOrHigher:reqSysVer];
}

BOOL LEXCurrentSystemVersionIsPriorToiOS7()
{
    return !LEXCurrentSystemVersionIsEqualOrHigher(@"7.0");
}