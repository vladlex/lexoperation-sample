//
//  LEXLoadAndColorImageView.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXLoadAndColorImageView.h"

@implementation LEXLoadAndColorImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        _colorPicker = [[UISegmentedControl alloc] init];
        [self addSubview:_colorPicker];
        
        _startButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_startButton setTitle:@"Start"
                      forState:UIControlStateNormal];
        [self addSubview:_startButton];
        
        _imageURLTextField = [[UITextField alloc] init];
        _imageURLTextField.placeholder = @"Image URL";
        [self addSubview:_imageURLTextField];
        
        _coloredImageView = [[UIImageView alloc] init];
        _coloredImageView.backgroundColor = [UIColor lightGrayColor];
        _coloredImageView.layer.cornerRadius = 5.0f;
        _coloredImageView.layer.masksToBounds = YES;
        _coloredImageView.contentMode = (UIViewContentModeScaleAspectFit);
        [self addSubview:_coloredImageView];
        
        _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        _spinner.hidesWhenStopped = YES;
        [self addSubview:_spinner];
    }
    return self;
}

- (void)setShouldBlockControlsAndShowSpinner:(BOOL)shouldBlockControlsAndShowSpinner
{
    if (_shouldBlockControlsAndShowSpinner != shouldBlockControlsAndShowSpinner) {
        _shouldBlockControlsAndShowSpinner = shouldBlockControlsAndShowSpinner;
        [self updateControlsBlocking];
    }
}

- (void)updateControlsBlocking
{
    BOOL enabled = !_shouldBlockControlsAndShowSpinner;
    _colorPicker.enabled = enabled;
    _colorPicker.userInteractionEnabled = enabled;
    _startButton.enabled = enabled;
    if (enabled) {
        [self.spinner stopAnimating];
    }
    else {
        [self.spinner startAnimating];
    }
}

- (void)layoutSubviews
{
    CGSize size = self.bounds.size;
    {
        CGSize tfSize = CGSizeMake(size.width - 16.0f, 44.0f);
        CGRect tfRect = CGRectMake(8.0f, 0.0f,
                                   tfSize.width, tfSize.height);
        _imageURLTextField.frame = tfRect;
    }   //< Image URL Text Field Layout
    {
        CGSize pickerSize = [_colorPicker sizeThatFits:CGSizeMake(size.width,
                                                                  0.0f)];
        CGRect pickerRect = CGRectMake((size.width - pickerSize.width) / 2,
                                       CGRectGetMaxY(_imageURLTextField.frame) + 4.0f,
                                       pickerSize.width,
                                       pickerSize.height);
        _colorPicker.frame = CGRectIntegral(pickerRect);
    }   //< Color Picker (UISegmentedControl) Layout
    {
        CGSize buttonSize = CGSizeMake(size.width - 44.0f,
                                       44.0f);
        CGRect buttonRect = CGRectMake( (size.width -  buttonSize.width) / 2,
                                       CGRectGetMaxY(_colorPicker.frame) + 4.0f,
                                       buttonSize.width,
                                       buttonSize.height);
        _startButton.frame = CGRectIntegral(buttonRect);
    }   //< Start Button Layout
    {
        CGRect area = CGRectMake(0.0f,
                                 0.0f,
                                 size.width,
                                 size.height - CGRectGetMaxY(_startButton.frame));
        UIEdgeInsets imageInsets = UIEdgeInsetsMake(8.0, 8.0f, 8.0f, 8.0f);
        CGSize imageViewSize = UIEdgeInsetsInsetRect(area, imageInsets).size;
        CGRect imageViewRect = CGRectMake(imageInsets.left,
                                          CGRectGetMaxY(_startButton.frame) + imageInsets.right,
                                          imageViewSize.width,
                                          imageViewSize.height);
        _coloredImageView.frame = imageViewRect;
    }
    {
        CGSize spinnerSize = self.spinner.bounds.size;
        if (CGSizeEqualToSize(spinnerSize, CGSizeZero))  {
            spinnerSize = CGSizeMake(32.0f, 32.0f);
        };
        _spinner.bounds = CGRectMake(0.0f, 0.0f,
                                     spinnerSize.width, spinnerSize.height);
        _spinner.center = _coloredImageView.center;
        _spinner.frame = CGRectIntegral(_spinner.frame);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
