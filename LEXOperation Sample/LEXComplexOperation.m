//
//  LEXComplexOperation.m
//
//  Created by Aleksei Gordeev on 28.02.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import "LEXComplexOperation.h"

@interface LEXComplexOperation()
@property (atomic, strong, readwrite) NSOperationQueue *actualSuboperationsQueue;
@property (atomic, strong, readwrite) dispatch_queue_t actualSuboperationsDispatchQueue;

- (void)setupActualQueues;

@end

@implementation LEXComplexOperation

- (void)setupActualQueues
{
    NSOperationQueue *opQueue = self.suboperationsQueue;
    if (!opQueue) {
        opQueue = [[NSOperationQueue alloc] init];
        NSString *name = [NSString stringWithFormat:@"com.lex.ops.%@.%@",
                          NSStringFromClass(self.class),
                          @(arc4random())];
        opQueue.name = name;
        opQueue.maxConcurrentOperationCount = 1;
    }
    self.actualSuboperationsQueue = opQueue;
    
    dispatch_queue_t opGCDQueue = self.suboperationsDispatchQueue;
    if (opGCDQueue == NULL || opGCDQueue == nil) {
        NSString *name = [NSString stringWithFormat:@"com.lex.GCDQueues.%@.%@",
                         NSStringFromClass(self.class),
                         @(arc4random())];
        opGCDQueue = dispatch_queue_create(name.UTF8String,
                                           NULL);
    }
    self.actualSuboperationsDispatchQueue = opGCDQueue;
}

- (void)operationDidStart
{
    [self setupActualQueues];
}

@end

@implementation LEXComplexOperation (Subclassing)

- (void)operationWillCancel
{
    [super operationWillCancel];
    [self.actualSuboperationsQueue cancelAllOperations];
}

- (void)addSuboperation:(LEXOperation *)operation
{
    operation.debugMode = self.debugMode;
    /** Operation should be run on special queue */
    if (!operation.dispatchQueue) {
        operation.dispatchQueue = self.actualSuboperationsDispatchQueue;
    }
    /** Operation should perfrom callbacks on main operation actual thread */
    if (!operation.callbacksDispatchQueue) {
        operation.callbacksDispatchQueue = self.actualDispatchQueue;
    }
    /** Additional configuring */
    [self willAddSuboperation:operation];
    /** Now operation configured and may be added to queue */
    [self.actualSuboperationsQueue addOperation:operation];
}

- (void)willAddSuboperation:(LEXOperation *)operation
{
    // Do nothing. Use it for overriding.
}

@end
