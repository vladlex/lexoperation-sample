//
//  LEXLoadingAngColoringImageOperation.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXLoadingAngColoringImageOperation.h"

#import "LEXURLConnectionDataLoadOperation.h"
#import "LEXImageColoringOperation.h"


NSString * const LEXLoadingAngColoringImageOperationErrorDomain = @"com.vladlex.errors.ops.loading_and_coloring";

@implementation NSError (LEXLoadingAngColoringImageOperationError)

- (BOOL)hasLEXLoadingAngColoringImageOperationErrorDomain
{
    return [self.domain isEqualToString:LEXLoadingAngColoringImageOperationErrorDomain];
}

+ (instancetype)loadingAndColoringImageWithCode:(NSInteger)code
                           localizedDescription:(NSString *)localizedDescription
{
    NSDictionary *userInfo = nil;
    if (localizedDescription) {
        userInfo = @{NSLocalizedDescriptionKey : localizedDescription};
    }
    return [NSError errorWithDomain:LEXLoadingAngColoringImageOperationErrorDomain
                               code:code
                           userInfo:userInfo];
}

@end

@implementation LEXLoadingAngColoringImageOperationResult
@dynamic coloredImage;

- (void)setColoredImage:(UIImage *)coloredImage
{
    [self setOutputData:coloredImage];
}

- (UIImage *)coloredImage
{
    return _outputData;
}


@end

@interface LEXLoadingAngColoringImageOperation ()
@property (nonatomic, strong) UIImage *sourceImage;

@end

@implementation LEXLoadingAngColoringImageOperation

+ (Class)resultClass
{
    return [LEXLoadingAngColoringImageOperationResult class];
}

- (id)init
{
    return [self initWithURLRequest:nil color:nil];
}

- (id)initWithURLRequest:(NSURLRequest *)request color:(UIColor *)color
{
    self = [super init];
    if (self != nil) {
        _request = request;
        _color = color;
    }
    return self;
}

- (void)operationDidStart
{
    [super operationDidStart];
 
    if (!self.request) {
        NSString *descr = @"No url request to load image";
        NSError *err = [NSError errorWithDomain:NSURLErrorDomain
                                           code:NSURLErrorBadURL
                                       userInfo:@{NSLocalizedDescriptionKey : descr}];
        [self finishWithFailAndError:err];
    }
    else {
        [self startImageLoading];
    }
}

- (void)startImageLoading
{
    LEXURLConnectionDataLoadOperation *op;
    NSURLRequest *request = self.request;
    op = [[LEXURLConnectionDataLoadOperation alloc] initWithURLRequest:request];
    op.dispatchQueue = dispatch_get_main_queue();
    
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained result) {
        [safeSelf didLoadImageWithResult:(id)result];
    }];
    [self addSuboperation:op];
}

- (void)didLoadImageWithResult:(LEXURLConnectionDataLoadOperationResult *)result
{
    BOOL success = result.success;
    if (success) {
        NSData *data = result.data;
        __weak typeof(self) safeSelf = self;
        dispatch_async(self.actualSuboperationsDispatchQueue, ^{
            UIImage *image = [[UIImage alloc] initWithData:data];
            if (!image) {
                NSInteger code = kLEXLoadingAngColoringImageOperationErrorFailToExecuteImageFromData;
                NSString *descr = @"Loaded data is not an image or has unknown format";
                NSError *err = [NSError loadingAndColoringImageWithCode:code
                                                   localizedDescription:descr];
                [safeSelf finishWithFailAndError:err];
            }
            else {
                safeSelf.sourceImage = image;
                if (self.color) {
                    dispatch_async(safeSelf.actualDispatchQueue, ^{
                        [safeSelf colorImage:image];
                    });
                }
                else {
                    [safeSelf finishWithSuccessAndOutputData:image];
                }
                
            }
        });
    }
    else {
        [self finishWithFailAndError:result.error];
    }
}

- (void)colorImage:(UIImage *)image
{
    LEXImageColoringOperation *op;
    op = [[LEXImageColoringOperation alloc] initWithImage:image
                                                    color:self.color];
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained result) {
        [safeSelf didColorImageWithResult:result];
    }];
    [self addSuboperation:op];
}

- (void)finishWithResult:(LEXOperationResult *)result
{
    // Appending operation result by source image.
    if ([result isKindOfClass:[LEXLoadingAngColoringImageOperationResult class]]) {
        [(id)result setSourceImage:self.sourceImage];
    }
    [super finishWithResult:result];
}

- (void)didColorImageWithResult:(LEXOperationResult *)result
{
    NSError *err = result.error;
    BOOL success = result.success;
    
    if (success && ![result.outputData isKindOfClass:[UIImage class]]) {
        success = NO;
        NSInteger code = kLEXLoadingAngColoringImageOperationErrorFailToColorImage;
        NSString *descr = @"Fail to color image for unknown reason";
        err = [NSError loadingAndColoringImageWithCode:code
                                  localizedDescription:descr];
    }
    if (success) {
        [self finishWithSuccessAndOutputData:result.outputData];
    }
    else {
        [self finishWithFailAndError:err];
    }
}

@end
