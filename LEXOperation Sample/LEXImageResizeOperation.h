//
//  LEXImageResizeOperation.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 17.07.14.
//
//

#import "LEXOperation.h"

@interface LEXImageResizeOperation : LEXOperation
@property (atomic, readonly) UIImage *image;
@property (atomic, readonly) CGSize targetSize;

/** Designated initializer */
- (id)initWithImage:(UIImage *)image targetSize:(CGSize)size;

- (id)initWithImage:(UIImage *)image scale:(CGFloat)scale;

- (id)initWithImage:(UIImage *)image sideLimit:(CGFloat)sideLimit;

@end
