//
//  LEXURLConnectionDataLoadOperation.h
//  TestApp
//
//  Created by Alexei Gordeev on 17.02.14.
//  Copyright (c) 2014 iTech. All rights reserved.
//

#import "LEXOperation.h"

#import "LEXOperationResult.h"

@interface LEXURLConnectionDataLoadOperationResult : LEXOperationResult
/** Upon server response contains NSURLResponse instance. */
@property (atomic, strong, readonly) NSURLResponse *response;

/** Success completion with given data and response */
- (id)initWithResponse:(NSURLResponse *)response
                  data:(NSData *)data;

- (NSData *)data;

/** Designated initializer */
- (id)initWithSuccess:(BOOL)success
                error:(NSError *)error
           outputData:(id)data
             response:(NSURLResponse *)response;

/** Check if class of response is NSURLHTTPResponse 
 @return YES if class of response is kind of NSURLHTTPResponse. */
- (BOOL)isResponseKindOfHTTPResponse;
/** Returns NSHTTPURLResponse object if response is kind of NSHTTPURLResponse class. 
 Otherwise returns nil. */
- (NSHTTPURLResponse *)httpResponse;

@end

/** LEXURLConnectionDataLoadOperation operation for data downloading.
 @warning This operation recommended to perform on main thread only. runLoopThread is main thread by default. Performing on main thread is NOT meaning that main thread is blocked untill operation finished. This thread is just used to recieve callbacks from connection and perform callbacks, but loading performs on background thread.
 */
@interface LEXURLConnectionDataLoadOperation : LEXOperation <NSURLConnectionDataDelegate>

// This parameter should be modified only before operation starts.
@property (atomic, strong, readonly) NSURLRequest *request;

@property (atomic, strong) NSRunLoop *runLoop;
@property (atomic, strong) NSArray *runLoopModes;

@property (atomic, assign, readonly) float currentProgress;

/** If varialbe not nil and not empty and code of request is unacceptable 
    then operation will fail with NSURLErrorBadServerResponse error 
    (NSURLErrorDomain). */
@property (nonatomic, copy) NSIndexSet *acceptableStatusCodes;

- (id)initWithURL:(NSURL *)URL;
- (id)initWithURLRequest:(NSURLRequest *)request;

- (NSData *)currentData;
- (NSURLResponse *)currentResponse;

- (LEXOperationProgressHandlerBlock)progressBlock;
- (void)setProgressBlock:(LEXOperationProgressHandlerBlock)progressBlock;

@end
