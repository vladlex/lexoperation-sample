//
//  LEXLoadingAndColoringImageViewController.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXLoadingAndColoringImageViewController.h"

#import "LEXLoadingAngColoringImageOperation.h"
#import "LEXImageColoringOperation.h"
#import "LEXLoadAndColorImageView.h"

#import "UIViewController+LEXHelpers.h"

@interface LEXLoadingAndColoringImageViewController ()
@property (nonatomic, readonly) LEXLoadAndColorImageView *contentView;
@property (nonatomic, readonly) NSOperationQueue *operationQueue;
@property (nonatomic) UIImage *sourceImage;

@end

@implementation LEXLoadingAndColoringImageViewController
@synthesize operationQueue = _operationQueue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        _colorAndTitles = @{@"Black (a 50%)":
                                [UIColor colorWithWhite:0.0f alpha:0.5f],
                            @"Green (a 50%)" :
                                [UIColor colorWithRed:0.0f green:1.0f blue:0.0f alpha:0.5f],
                            @"Red (a 50%)" :
                                [UIColor colorWithRed:1.0f green:0.0f blue:0.0f alpha:0.5f]};
    }
    return self;
}

- (NSOperationQueue *)operationQueue
{
    if (!_operationQueue) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.name = [NSString stringWithFormat:@""
                                "com.vladlex.sample.%@Queue",
                                NSStringFromClass(self.class)];
    }
    return _operationQueue;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self prepareForExtendedLayoutWithNoEdges];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _contentView = [[LEXLoadAndColorImageView alloc] initWithFrame:self.view.bounds];
    NSArray *titles = [_colorAndTitles.allKeys sortedArrayUsingSelector:@selector(localizedCompare:)];
    [titles enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id obj,
                                                                          NSUInteger idx,
                                                                          BOOL *stop) {
        [_contentView.colorPicker insertSegmentWithTitle:obj
                                                 atIndex:0
                                                animated:NO];
    }];
    if (LEXCurrentSystemVersionIsPriorToiOS7()) {
        NSDictionary *attrs = [_contentView.colorPicker titleTextAttributesForState:UIControlStateNormal];
        NSMutableDictionary *updAttrs = [attrs mutableCopy];
        if (!updAttrs ) {
            updAttrs = [NSMutableDictionary dictionary];
        }
        updAttrs[UITextAttributeFont] = [UIFont systemFontOfSize:12.0f];
        [_contentView.colorPicker setTitleTextAttributes:updAttrs
                                                forState:UIControlStateNormal];
    }
    
    [_contentView.colorPicker setSelectedSegmentIndex:0];
    [_contentView.colorPicker addTarget:self
                                 action:@selector(pickedColorDidChange:)
                       forControlEvents:UIControlEventValueChanged];
    [_contentView.startButton addTarget:self
                                 action:@selector(startImageLoadingAndColoringAction:)
                       forControlEvents:UIControlEventTouchUpInside];
    _contentView.autoresizingMask = (UIViewAutoresizingFlexibleHeight |
                                     UIViewAutoresizingFlexibleWidth);
    
    _contentView.imageURLTextField.text = @"http://itechstore.ru/1.jpg";
    [self.view addSubview:_contentView];
    
    self.title = @"Image Loading (+ Color)";
}

- (UIColor *)pickedColor
{
    UISegmentedControl *picker = _contentView.colorPicker;
    NSString *title = [picker titleForSegmentAtIndex:picker.selectedSegmentIndex];
    return _colorAndTitles[title];
}

- (void)pickedColorDidChange:(id)sender
{
    UIImage *image = self.sourceImage;
    if (!image) {
        return;
    }
    [self colorImage:image withColor:self.pickedColor];
}

- (void)colorImage:(UIImage *)image withColor:(UIColor *)color
{
    if (self.operationQueue.operationCount != 0) {
        return;
    }
    _contentView.shouldBlockControlsAndShowSpinner = YES;
    LEXImageColoringOperation *op;
    op = [[LEXImageColoringOperation alloc] initWithImage:image color:color];
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained result) {
        [safeSelf didColorImage:result.outputData
                    sourceImage:image
                       imageURL:nil
                          color:color
                        success:result.success
                          error:result.error];
    }];
    [self.operationQueue addOperation:op];
}

- (void)startImageLoadingAndColoringAction:(id)sender
{
    [self.contentView endEditing:YES];
    NSString *link = _contentView.imageURLTextField.text;
    NSURL *imageURL = [NSURL URLWithString:link];
    if (!imageURL) {
        NSString *msg = @"Image URL should be valid";
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
                                                     message:msg
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil];
        [av show];
        return;
    }
    
    [self loadImageFromURL:imageURL color:self.pickedColor];
}


- (void)loadImageFromURL:(NSURL *)imageURL
                   color:(UIColor *)color
{
    if (self.operationQueue.operationCount != 0) {
        NSLog(@"Already executing operation");
        return;
    }
    _contentView.shouldBlockControlsAndShowSpinner = YES;
    NSAssert(imageURL, @"%@ : %@. Should have image url to load image",
             NSStringFromClass(self.class), NSStringFromSelector(_cmd));
    LEXLoadingAngColoringImageOperation *op;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:imageURL
                                             cachePolicy:NSURLRequestReloadIgnoringCacheData
                                         timeoutInterval:30.0];
    op = [[LEXLoadingAngColoringImageOperation alloc] initWithURLRequest:request
                                                                   color:color];
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
        LEXLoadingAngColoringImageOperationResult *result = (id)r;
        [safeSelf didColorImage:result.coloredImage
                    sourceImage:result.sourceImage
                       imageURL:imageURL
                          color:color
                        success:result.success
                          error:result.error];
    }];
    
    // This is unnecesssary. By default callbacks queue is always main queue.
    op.callbacksDispatchQueue = dispatch_get_main_queue();
    
    [self.operationQueue addOperation:op];
}

- (void)didColorImage:(UIImage *)image
          sourceImage:(UIImage *)sourceImage
             imageURL:(NSURL *)imageURL
                color:(UIColor *)color
              success:(BOOL)success
                error:(NSError *)error
{
    self.sourceImage = sourceImage;
    self.contentView.shouldBlockControlsAndShowSpinner = NO;
    if (success && !image) {
        success = NO;
        error = [NSError errorWithDomain:@"com.vladlex.controller_domain"
                                    code:-1
                                userInfo:@{NSLocalizedDescriptionKey:
                                               @"Undefined error. No image."}];
    }
    _contentView.coloredImageView.image = image;
    if (!success) {
        UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
                                                     message:error.localizedDescription
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil];
        [av show];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
