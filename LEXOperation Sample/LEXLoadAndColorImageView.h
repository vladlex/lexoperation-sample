//
//  LEXLoadAndColorImageView.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import <UIKit/UIKit.h>

@interface LEXLoadAndColorImageView : UIView
@property (nonatomic, readonly) UISegmentedControl *colorPicker;
@property (nonatomic, readonly) UITextField *imageURLTextField;
@property (nonatomic, readonly) UIButton *startButton;
@property (nonatomic, readonly) UIImageView *coloredImageView;
@property (nonatomic, readonly) UIActivityIndicatorView *spinner;

@property (nonatomic) BOOL shouldBlockControlsAndShowSpinner;

@end
