//
//  LEXLoadingAndColoringImageViewController.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import <UIKit/UIKit.h>

@interface LEXLoadingAndColoringImageViewController : UIViewController
{
    @protected
    NSDictionary *_colorAndTitles;
}
/** Loading and coloring image
 @property imageURL URL of image. Should not be nil.
 @property color Image overlay color. May be nil.
 */
- (void)loadImageFromURL:(NSURL *)imageURL color:(UIColor *)color;

- (UIColor *)pickedColor;

@end
