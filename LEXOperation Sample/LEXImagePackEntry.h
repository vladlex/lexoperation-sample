//
//  LEXImagePackEntry.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 16.07.14.
//
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, LEXImagePackEntryState)
{
    kLEXImagePackEntryStateNotLoaded = 0,
    kLEXImagePackEntryStateLoading,
    kLEXImagePackEntryStateLoaded,
    kLEXImagePackEntryStateFailed
};

@interface LEXImagePackEntry : NSObject
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) NSURL *sourceURL;
@property (nonatomic, assign) LEXImagePackEntryState state;

@property (nonatomic, strong) NSError *error;

/** Value in 0-1 range*/
@property (nonatomic, assign) float loadingProgress;

+ (instancetype)entryWithLink:(NSString *)link;

- (id)initWithSourceURL:(NSURL *)URL;

- (BOOL)isLoadingFinished;

- (void)configureWithLoadedImage:(UIImage *)image;
- (void)configureWithFailError:(NSError *)error;

@end
