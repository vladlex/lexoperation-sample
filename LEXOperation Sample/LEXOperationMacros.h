//
//  LEXOperationMacros.h
//  TestApp
//
//  Created by Aleksei Gordeev on 17.02.14.
//  Copyright (c) 2014 iTech. All rights reserved.
//

/** @file LEXOperationMacros.h
 * Useful macros for overriding and subclassing LEXOperation
 *
 */


#ifndef TestApp_LEXOperationMacros_h
#define TestApp_LEXOperationMacros_h


#ifdef DEBUG
/** Comment this line to avoid of queue checking even in debug mode.
 This macro is for debugging purposes only and will not be used in production.
 */
    #define LEXOperationDebug
#endif

// _Pragma used to avoid of clang 'deprecated' warnings.
// Deprecated method 'dispatch_get_current_queue()' used only for debugging.
// This method will no be called by this macro in production.

// If we in Debug mode and in LEXOperationDebug mode then we can check actual queue.

/** Check if current queue is actual queue.
 @warning This macro is for debug purposes only (see dispatch_get_current_queue() deprecation)
 */

#ifdef LEXOperationDebug
    #define LEXOperationCheckIfQueueIsCurrent(queue, successCode, failureCode)\
    {\
        _Pragma("clang diagnostic push") \
        _Pragma("clang diagnostic ignored \"-Wdeprecated-declarations\"") \
        dispatch_queue_t deprecated_current_queue = dispatch_get_current_queue(); \
        _Pragma("clang diagnostic pop") \
        if (deprecated_current_queue != queue){ \
            failureCode\
        }\
        else {\
            successCode\
        }\
    }

    #define LEXOperationWrongQueueLogString \
    [NSString stringWithFormat:@"[%@ %@] called on wrong dispatch queue (%s: %d)",\
    NSStringFromClass(self.class), NSStringFromSelector(_cmd), __FILE__, __LINE__ ]

    #define LEXOperationCheckCurrentQueueIsActual \
    LEXOperationCheckIfQueueIsCurrent(self.actualDispatchQueue, , NSLog(@"%@", LEXOperationWrongQueueLogString););

#else
    // LEXOperationDebug mode disabled. Do nothing.
    #define LEXOperationCheckIfQueueIsCurrent(queue, successCode, failureCode)
    #define LEXOperationCheckCurrentQueueIsActual
#endif


#define LEXOperationStopIfFinished if (self.isFinished) return

/** Return from current method if operation was cancelled */
#define LEXOperationStopIfCancelled if (self.isCancelled) return

/** Asserts if operation is not configurable */
#define LEXOperationAssertIfNotConfigurable(string) NSAssert(self.isConfigurable, @"%@", string);


#endif
