//
//  LEXRequestIPViewController.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 05.05.14.
//
//

#import "LEXRequestIPViewController.h"

#import "UIViewController+LEXHelpers.h"
#import "LEXJSONOpView.h"
#import "LEXURLConnectionDataLoadOperation.h"

@interface LEXRequestIPViewController ()
@property (nonatomic) LEXJSONOpView *mainView;
@property (nonatomic) NSOperationQueue *operationQueue;

@end

@implementation LEXRequestIPViewController
@synthesize operationQueue = _operationQueue;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self prepareForExtendedLayoutWithNoEdges];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [self setupMainView];
}

- (void)setupMainView
{
    [_mainView removeFromSuperview];
    LEXJSONOpView *view = [[LEXJSONOpView alloc] initWithFrame:self.view.bounds];
    view.autoresizingMask = (UIViewAutoresizingFlexibleHeight |
                             UIViewAutoresizingFlexibleWidth);
    [view.requestIPButton addTarget:self
                             action:@selector(requestIPAction:)
                   forControlEvents:UIControlEventTouchUpInside];
    _mainView = view;
    [self.view addSubview:view];
}

- (NSOperationQueue *)operationQueue
{
    if (!_operationQueue) {
        _operationQueue = [[NSOperationQueue alloc] init];
        _operationQueue.name = [NSString stringWithFormat:@""
                                "com.vladlex.sample.%@Queue",
                                NSStringFromClass(self.class)];
    }
    return _operationQueue;
}

- (void)requestIPAction:(id)sender
{
    _mainView.requestIPButton.enabled = NO;
    
    NSURL *URL;
    LEXURLConnectionDataLoadOperation *op;
    
    URL = [NSURL URLWithString:@"http://ip.jsontest.com"];
    op = [[LEXURLConnectionDataLoadOperation alloc] initWithURL:URL];
    
    // Uncomment following line for detailed execution logs.
    // op.debugMode = YES;
    
    __weak typeof(self) safeSelf = self;
    [op setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
        [safeSelf processRequestResult:(id)r];
    }];
    [self.operationQueue addOperation:op];
}

- (void)processRequestResult:(LEXURLConnectionDataLoadOperationResult *)result
{
    _mainView.requestIPButton.enabled = YES;
    
    // User deserves to know failure reason, so we must to provide an error.
    
    NSError *error = result.error;
    BOOL success = result.success;
    if (success && ![result isResponseKindOfHTTPResponse]) {
        NSString *descr = @"Expecting response using HTTP protocol";
        error = [NSError errorWithDomain:NSURLErrorDomain
                                    code:NSURLErrorBadServerResponse
                                userInfo:@{NSLocalizedDescriptionKey :
                                               descr}];
        success = NO;
    }
    if (success && result.data.length == 0) {
        NSString *descr = @"Server returns no data";
        error = [NSError errorWithDomain:NSURLErrorDomain
                                    code:NSURLErrorBadServerResponse
                                userInfo:@{NSLocalizedDescriptionKey :
                                               descr}];
        success = NO;
    }
    if (success) {
        NSString *string = [[NSString alloc] initWithData:result.data
                                                 encoding:NSUTF8StringEncoding];
        NSString *ip = [self ipStringFromIPJSONResponse:string];
        if (ip.length > 0) {
            self.mainView.ipLabel.text = ip;
            self.title = ip;
            [self.mainView setNeedsLayout];
            return;
        }
        else {
            // Fail to execute ip. Data is corrupted.
            NSString *descr = @"Data is possibly corrupted";
            error = [NSError errorWithDomain:NSURLErrorDomain
                                        code:NSURLErrorBadServerResponse
                                    userInfo:@{NSLocalizedDescriptionKey :
                                                   descr}];
        }
    }
    
    if (!error) {
        // If there is no error user still deserves to know some information about failure.
        NSString *descr = @"Some unknown error happens";
        error = [NSError errorWithDomain:NSCocoaErrorDomain
                                    code:-1
                                userInfo:@{NSLocalizedDescriptionKey : descr}];
    }
    UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error"
                                                 message:error.localizedDescription
                                                delegate:nil
                                       cancelButtonTitle:@"Ok"
                                       otherButtonTitles:nil];
    [av show];
    self.title = @"IP request";
}

/// Returns ip string if success and nil if something goes wrong.
- (NSString *)ipStringFromIPJSONResponse:(NSString *)ipResponse
{
    if (ipResponse.length == 0) {
        return nil;
    }
    
    /// Final ip result string. May be nil.
    NSString *result = nil;
    
    // Normally you use any JSON serialization here. For iOS 7 and higher
    //  it's usually NSJSONSerialization and for olader verions it's something like JSONKit.
    // But in this project I don't want to import any third-party classes.
    
    // We can use regular expressions but let's try to keep code more dev-friendly.
    
    if ([ipResponse hasSuffix:@"\n"]) {
        ipResponse = [ipResponse substringToIndex:ipResponse.length - 1];
    }
    if ([ipResponse hasPrefix:@"{"] && [ipResponse hasSuffix:@"}"]) {
        /// Key-value array
        NSRange JSONBodyRange = NSMakeRange(1, ipResponse.length - 2);
        ipResponse = [ipResponse substringWithRange:JSONBodyRange];
        NSArray *kvPairs = [ipResponse componentsSeparatedByString:@": "];
        if (kvPairs.count > 1) {
            NSString *key = kvPairs[0];
            // Check if key is "ip"
            if ([key caseInsensitiveCompare:@"\"ip\""] == NSOrderedSame) {
                // That's it. We get ip!
                result = kvPairs[1];
                if (result.length > 2 &&
                    [result hasPrefix:@"\""] &&
                    [result hasSuffix:@"\""]) {
                    result = [result substringWithRange:NSMakeRange(1, result.length - 2)];
                }
            }
        }
    }
    return result;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAll;
}

@end
