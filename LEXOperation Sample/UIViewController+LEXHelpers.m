//
//  UIViewController+LEXHelpers.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "UIViewController+LEXHelpers.h"

@implementation UIViewController (LEXHelpers)

- (void)prepareForExtendedLayoutWithNoEdges
{
    if ([[UIDevice currentDevice] isSystemVersionEqualOrHigher:@"7.0"]) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.automaticallyAdjustsScrollViewInsets = YES;
    }
}

@end
