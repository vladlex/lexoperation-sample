//
//  main.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 05.05.14.
//
//

#import <UIKit/UIKit.h>

#import "LEXAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([LEXAppDelegate class]));
    }
}
