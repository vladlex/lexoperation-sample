//
//  UIDevice+LEXSystemVersion.h
//
//  Created by Aleksei Gordeev on 21.02.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIDevice (LEXSystemVersion)

/** Returns an array of NSNumber instances. If version was 4.2 then result array will be {@(4), @(2)}. */
+ (NSArray *)systemVersionComponents;
+ (NSArray *)systemVersionComponentsFromSystemVersion:(NSString *)systemVersion;

+ (NSComparisonResult)compareCurrentSystemVersionWithVersion:(NSString *)version;
+ (NSComparisonResult)compareSystemVersion:(NSString *)version1
                                 toVersion:(NSString *)version2;
/** Compare versions in "v1.v2.v3..." format.
 @warning Be careful with RIGHT NSComparisonResult understanding. See apple docs.
 NSOrderedAscending means that version1 > version2. NSOrderedDescending means that version1 < version2.
 @param version1 Left compare operand
 @param version2 Right compare operand
 @param difference Uponr return contains difference in version.
 @param index Upon return contains index of difference.
 @brief You can easily get first found difference. I. e. version1 is 6.0 and version2 is 7.0 then difference will be 1 and index is 0. I. e. version1 is 5.0.3 and version2 is 5.2.0 difference is 2 and index is 1.
 */
+ (NSComparisonResult)compareSystemVersion:(NSString *)version1
                                 toVersion:(NSString *)version2
                             getDifference:(out NSUInteger *)difference
                                     index:(out NSUInteger *)index;
/**
    Returns 'YES' if system version is equal or higher than required. 
 Results are caching to avoid duplicate calculations.
 @param requiredSystemVersion required system version minimum.
 @return YES' if system version is equal or higher than required.
 */
- (BOOL)isSystemVersionEqualOrHigher:(NSString *)requiredSystemVersion;

@end

BOOL LEXCurrentSystemVersionIsEqualOrHigher(NSString *reqSysVer);
BOOL LEXCurrentSystemVersionIsPriorToiOS7();