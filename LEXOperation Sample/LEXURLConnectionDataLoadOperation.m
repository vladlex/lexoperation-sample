//
//  LEXURLConnectionDataLoadOperation.m
//  TestApp
//
//  Created by Alexei Gordeev on 17.02.14.
//  Copyright (c) 2014 iTech. All rights reserved.
//

#import "LEXURLConnectionDataLoadOperation.h"

#import "LEXOperationMacros.h"

@interface LEXURLConnectionDataLoadOperationResult ()
@property (atomic, strong, readwrite) NSURLResponse *response;

@end

@implementation LEXURLConnectionDataLoadOperationResult

- (void)setOutputData:(id)data
{
    if (data == nil) {
        _outputData = data;
    }
    else {
        NSAssert([data isKindOfClass:[NSData class]],
                 @"Output data for %@ should be kind of NSData class",
                 NSStringFromClass(self.class));
        _outputData = [data copy];
    }
}

- (id)initWithResponse:(NSURLResponse *)response
                  data:(NSData *)data
{
    return [self initWithSuccess:YES
                           error:nil
                      outputData:data
                        response:response];
}

- (id)initWithSuccess:(BOOL)success
                error:(NSError *)error
           outputData:(id)data
{
    return [self initWithSuccess:success
                           error:error
                      outputData:data
                        response:nil];
}

- (id)initWithSuccess:(BOOL)success
                error:(NSError *)error
           outputData:(id)data
             response:(NSURLResponse *)response
{
    self = [super initWithSuccess:success
                           error:error
                      outputData:data];
    if (self != nil) {
        self.response = response;
    }
    return self;
}

- (BOOL)isResponseKindOfHTTPResponse
{
    return [_response isKindOfClass:[NSHTTPURLResponse class]];
}

- (NSHTTPURLResponse *)httpResponse
{
    if ([self isResponseKindOfHTTPResponse]) {
        return (id)_response;
    }
    return nil;
}

- (NSData *)data
{
    return self.outputData;
}

@end

@interface LEXURLConnectionDataLoadOperation () 
@property (nonatomic, copy) LEXOperationProgressHandlerBlock progressBlock;

@property (nonatomic, strong) NSURLConnection *connection;
@property (atomic, strong) NSMutableData *dataAccumulator;

@property (atomic, strong) NSURLResponse *temporaryResponse;
@property (nonatomic, assign) long long expectedDataLength;

@property (atomic, assign, readwrite) float currentProgress;



- (BOOL)isStatusCodeAcceptable:(NSInteger)code;
- (void)recalculateProgress;
- (void)declareProgress;
- (void)finishWithSuccess;
- (void)setupDataAccumulatorWithLength:(NSUInteger)length;
- (void)didRecieveDataWithCorrectQueue:(NSData *)data;

- (void)destroyConnection;

@end

@implementation LEXURLConnectionDataLoadOperation

#pragma mark Fast error creation

static inline NSError * LEXBadURLError(NSString *localizedDescription,
                                       NSURL *URL)
{
    if (!localizedDescription) {
        localizedDescription = [NSString stringWithFormat:@"Bad URL: %@",
                                URL.path];
    }
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: localizedDescription};
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:NSURLErrorBadURL
                           userInfo:userInfo];
}

static inline NSError * LEXUnknownError(NSString *explanation)
{
    if (!explanation) {
        explanation = @"Unknown error";
    }
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: explanation};
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:NSURLErrorUnknown
                           userInfo:userInfo];
}

static inline NSError * LEXUnacceptableCodeError(NSInteger code,
                                                 NSIndexSet *codes)
{
    NSString *descr = [NSString stringWithFormat:@"Response status code "
                       "is unacceptable (%ld)", (long)code];
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: descr};
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:NSURLErrorBadServerResponse
                           userInfo:userInfo];
}

static inline NSError *LEXLengtExceedLimitError(long long length)
{
    NSString *descr = [NSString stringWithFormat:@"Expecting data length"
                       "is too long: %lld MB",
                       length / 1024 / 1024];
    NSDictionary *userInfo = @{NSLocalizedDescriptionKey: descr};
    return [NSError errorWithDomain:NSURLErrorDomain
                               code:NSURLErrorDataLengthExceedsMaximum
                           userInfo:userInfo];
}

#pragma mark Life Cycle

- (void)dealloc
{
    [self destroyConnection];
}

- (id)init
{
    return [self initWithURL:nil];
}

- (id)initWithURL:(NSURL *)URL
{
    return [self initWithURLRequest:[NSURLRequest requestWithURL:URL]];
}

- (id)initWithURLRequest:(NSURLRequest *)request
{
    self = [super init];
    if (self != nil) {
        self.expectedDataLength = NSURLResponseUnknownLength;
        self.request = request;
        self.dispatchQueue = dispatch_get_main_queue();
        self.callbacksDispatchQueue = dispatch_get_main_queue();
    }
    return self;
}

- (void)setRequest:(NSURLRequest *)request
{
    LEXOperationAssertIfNotConfigurable(@"Request can be modified only before operation execution");
    if (_request != request) {
        _request = request;
    }
}

#pragma mark Override

+ (Class)resultClass
{
    return [LEXURLConnectionDataLoadOperationResult class];
}

- (void)operationWillCancel
{
    [self.connection cancel];
    self.connection = nil;
    [super operationWillCancel];
}

- (void)operationDidStart
{
    LEXOperationCheckCurrentQueueIsActual;
    [super operationDidStart];
    NSURLRequest *request = self.request;
    NSURLConnection *connection;
    connection = [[NSURLConnection alloc] initWithRequest:request
                                                 delegate:self
                                         startImmediately:NO];
    if (!connection || !request) {
        [self finishWithFailAndError:LEXBadURLError(nil, request.URL)];
        return;
    }
    NSRunLoop *runLoop = self.runLoop;
    if (!runLoop) {
        runLoop = [NSRunLoop mainRunLoop];
    }
    NSArray *modes = self.runLoopModes;
    if (!modes) {
        modes = @[NSRunLoopCommonModes];
    }
    for (NSString *runLoopMode in modes) {
        [connection scheduleInRunLoop:runLoop
                              forMode:runLoopMode];
    }
    self.connection = connection;

    
    [connection start];
}

- (void)cleanUp
{
    [self.connection cancel];
    self.connection = nil;
    self.temporaryResponse = nil;
    [super cleanUp];
}

- (void)recalculateProgress
{
    LEXOperationStopIfCancelled;
    if (self.isFinished) {
        return;
    }
    if (_expectedDataLength != NSURLResponseUnknownLength &&
        _expectedDataLength != 0) {
        self.currentProgress = self.dataAccumulator.length / _expectedDataLength;
        __weak typeof(self) safeSelf = self;
        dispatch_async(self.actualCallbacksDispatchQueue, ^{
            [safeSelf declareProgress];
        });
    }
}



#pragma mark Private methods

- (void)destroyConnection
{
    if (self.connection) {
        [self.connection cancel];
        self.connection = nil;
    }
}

- (void)setupDataAccumulatorWithLength:(NSUInteger)length
{
    self.dataAccumulator = [NSMutableData dataWithCapacity:length];
}

- (BOOL)isStatusCodeAcceptable:(NSInteger)code
{
    NSIndexSet *codes = self.acceptableStatusCodes;
    if (!codes) {
        return YES;
    }
    return [codes containsIndex:code];
}

- (NSData *)currentData
{
    if (_dataAccumulator) {
        return _dataAccumulator.copy;
    }
    else {
        return nil;
    }
}

- (NSURLResponse *)currentResponse
{
    return self.temporaryResponse;
}

- (void)finishWithSuccess
{
    LEXURLConnectionDataLoadOperationResult *result;
    Class ResultClass = [self.class resultClass];
    result = [[ResultClass alloc] initWithResponse:self.temporaryResponse
                                              data:self.dataAccumulator.copy];
    [self destroyConnection];
    [self finishWithResult:result];
}

- (void)declareProgress
{
    LEXOperationCheckIfQueueIsCurrent(self.actualCallbacksDispatchQueue,
                                      {};,
                                      NSLog(@"Wrong queue"););
    if (self.isFinished) {
        return;
    }
    float progress = self.currentProgress;
    NSURLResponse *response = self.temporaryResponse;
    [self declareProgress:progress userInfo:response];
}

- (void)didRecieveDataWithCorrectQueue:(NSData *)data
{
    LEXOperationCheckCurrentQueueIsActual;
    [self.dataAccumulator appendData:data];
    [self recalculateProgress];
}

- (void)finishWithResult:(LEXOperationResult *)result
{
    [super finishWithResult:result];
}

#pragma mark NSURLConnectionDataDelegate

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    if (connection == _connection) {
        if (!error) {
            NSString *explanation;
            explanation = [NSString stringWithFormat:@"%@ with empty error",
                           NSStringFromSelector(_cmd)];
            error = LEXUnknownError(explanation);
        }
        [self destroyConnection];
        [self finishWithFailAndError:error];
        return;
    }
}

- (void)connection:(NSURLConnection *)connection
didReceiveResponse:(NSURLResponse *)response
{
    if (self.isInDebugMode) {
        NSLog(@"Did recieve response: %@", response);
    }
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        NSHTTPURLResponse *httpResponse = (id)response;
        NSInteger code = httpResponse.statusCode;
        BOOL codeIsAcceptable = [self isStatusCodeAcceptable:code];
        if (!codeIsAcceptable) {
            NSIndexSet *codes = self.acceptableStatusCodes;
            [self destroyConnection];
            [self finishWithFailAndError:LEXUnacceptableCodeError(code,
                                                                  codes)];
            return;
        }
    }
    long long dataLength = response.expectedContentLength;
    if (dataLength != NSURLResponseUnknownLength) {
        if (dataLength > (long long)NSUIntegerMax) {
            [self finishWithFailAndError:LEXLengtExceedLimitError(dataLength)];
            return;
        }
        else {
            [self setupDataAccumulatorWithLength:(NSUInteger)dataLength];
            self.expectedDataLength = dataLength;
        }
    }
    else {
        self.dataAccumulator = [[NSMutableData alloc] init];
    }
    self.temporaryResponse = response;
    [self recalculateProgress];
}

- (void)connection:(NSURLConnection *)connection
    didReceiveData:(NSData *)data
{
    [self.dataAccumulator appendData:data];
    dispatch_async(self.actualDispatchQueue, ^{
        [self recalculateProgress];
    });
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    LEXOperationStopIfCancelled;
    [self destroyConnection];
    [self finishWithSuccess];
}

- (NSString *)description
{
    NSString *main = [NSString stringWithFormat:@"%@: %p",
                      NSStringFromClass(self.class), self];
    NSString *URL = [NSString stringWithFormat:@"URL: %@",
                     self.request.URL.path];
    
    NSArray *info = @[main, URL];
    if (self.title) {
        NSString *title = [NSString stringWithFormat:@"title: %@",
                           self.title];
        info = [info arrayByAddingObject:title];
    }
    return [NSString stringWithFormat:@"<%@>",
            [info componentsJoinedByString:@", "]];
}

@end
