//
//  LEXJSONOpView.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 05.05.14.
//
//

#import <UIKit/UIKit.h>

/** View presenting request button and request result. All controls are centered in view. */
@interface LEXJSONOpView : UIView

/** Label for IP representation */
@property (nonatomic) UILabel *ipLabel;
/** Request button. 
 According to MVC this button has not any predefined target/actions */
@property (nonatomic) UIButton *requestIPButton;

/** Content insets. 8pt-all-sides insets by default.
 Value changing automatically leads to 'setNeedsLayout'. */
@property (nonatomic, assign) UIEdgeInsets contentInsets;
/** Margin from ip label to request button. 8.0 points by default. */
@property (nonatomic, assign) CGFloat labelToButtonMargin;

@end
