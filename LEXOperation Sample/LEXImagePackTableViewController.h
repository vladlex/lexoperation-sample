//
//  LEXImagePackTableViewController.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 16.07.14.
//
//

#import <UIKit/UIKit.h>


/**
 This controller showing image pack loading.
 Feel free to edit ImageLinks.plist for modifying image links.
 */
@interface LEXImagePackTableViewController : UITableViewController
{
    @protected
    dispatch_queue_t _complexOpsQueue;
}
/// An array of LEXImagePackEntry instances
@property (nonatomic, strong) NSArray *imageEntries;

@property (nonatomic, assign) BOOL imageShouldBeResized;

- (void)reloadImages:(id)sender;

@end
