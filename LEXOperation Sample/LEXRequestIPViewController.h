//
//  LEXRequestIPViewController.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 05.05.14.
//
//

#import <UIKit/UIKit.h>

// See this class to understand what exactly this controller presents. Yes, I like to create interface programmatically.
@class LEXJSONOpView;

/** This controller presents view fof sending request to server and shows device ip. */
@interface LEXRequestIPViewController : UIViewController

@end
