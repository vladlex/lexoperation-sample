//
//  LEXImagePackTableViewController.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 16.07.14.
//
//

#import "LEXImagePackTableViewController.h"

#import "LEXImagePackEntry.h"
#import "LEXURLConnectionDataLoadOperation.h"
#import "LEXLoadingAndResizingImageOperation.h"

@interface LEXImagePackTableViewController ()
@property (nonatomic, strong) NSOperationQueue *opQueue;

@end

const CGFloat kLEXImageLimitSide = 44.0;

@implementation LEXImagePackTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
        
        _opQueue = [NSOperationQueue new];
        _opQueue.name = @"com.lex.op_sample.imagepack_loading";
        _opQueue.maxConcurrentOperationCount = 3;
        
        _complexOpsQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW,
                                                     0);
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupImageEntries];
    [self setupReloadItem];
}

- (void)setupImageEntries
{
    NSURL *linksListURL = [[NSBundle mainBundle] URLForResource:@"ImageLinks"
                                                  withExtension:@"plist"];
    NSData *data = [NSData dataWithContentsOfURL:linksListURL];
    NSError *error = nil;
    NSArray *links = [NSPropertyListSerialization propertyListWithData:data
                                                               options:0
                                                                format:NULL
                                                                 error:&error];
    if (!links) {
        UIAlertView *av;
        NSString *avTitle;
        
        avTitle = [NSString stringWithFormat:@""
                   "Fail to load image links. Error: %@",
                   error.localizedDescription];
        av = [[UIAlertView alloc] initWithTitle:avTitle
                                        message:@""
                                       delegate:nil
                              cancelButtonTitle:@"Ok"
                              otherButtonTitles:nil];
        [av show];
    }
    else {
        NSMutableArray *imageEntries;
        LEXImagePackEntry *anEntry;
        
        imageEntries = [NSMutableArray arrayWithCapacity:links.count];
        for (NSString *link in links) {
            anEntry = [LEXImagePackEntry entryWithLink:link];
            if (anEntry) {
                [imageEntries addObject:anEntry];
            }
        }
        self.imageEntries = imageEntries;
    }
}

- (void)setupReloadItem
{
    UIBarButtonItem *item;
    UIBarButtonSystemItem sysItem = UIBarButtonSystemItemRefresh;
    SEL action = @selector(reloadImages:);
    item = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:sysItem
                                                         target:self
                                                         action:action];
    self.navigationItem.rightBarButtonItem = item;
}

- (void)reloadImages:(id)sender
{
    [self setupImageEntries];
}

- (void)setImageEntries:(NSArray *)imageEntries
{
    if (_imageEntries != imageEntries) {
        _imageEntries = imageEntries;
        [_opQueue cancelAllOperations];
        [self.tableView reloadData];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setImageShouldBeResized:(BOOL)imageShouldBeResized
{
    if (_imageShouldBeResized != imageShouldBeResized) {
        _imageShouldBeResized = imageShouldBeResized;
        [self setupImageEntries];
    }
}

- (LEXOperation *)operationWitSourceURL:(NSURL *)URL
{
    NSArray *ops = self.opQueue.operations;
    for (LEXOperation *op in ops) {
        if ([op isKindOfClass:[LEXLoadingAndResizingImageOperation class]]) {
            LEXLoadingAndResizingImageOperation *cOp = (id)op;
            if ([cOp.URL isEqual:URL]) {
                return cOp;
            }
        }
        else if ([op isKindOfClass:[LEXURLConnectionDataLoadOperation class]]) {
            LEXURLConnectionDataLoadOperation *cOp = (id)op;
            if ([cOp.request.URL isEqual:URL]) {
                return cOp;
            }
        }
    }
    return nil;
}

- (LEXImagePackEntry *)imageEntryWithSourceURL:(NSURL *)URL
{
    NSArray *entries = self.imageEntries;
    for (LEXImagePackEntry *entry in entries) {
        if ([entry.sourceURL isEqual:URL]) {
            return entry;
        }
    }
    return nil;
}

- (LEXOperation *)startImageLoadingOperationWithEntry:(LEXImagePackEntry *)entry
                                    shouldResizeImage:(BOOL)resizeImage
{
    __weak LEXImagePackEntry *weakEntry = entry;
    LEXOperation *op;
    NSURL *URL = entry.sourceURL;
    __weak typeof(self) safeSelf = self;
    if (!resizeImage) {
        /// Concrete op (Load without resizing)
        LEXURLConnectionDataLoadOperation *cOp;
        cOp = [[LEXURLConnectionDataLoadOperation alloc] initWithURL:URL];
        cOp.runLoop = nil;
        cOp.runLoopModes = nil;
        [cOp setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
            LEXURLConnectionDataLoadOperationResult *result = (id)r;
            UIImage *image = nil;
            if (result.success) {
                image = [UIImage imageWithData:result.data];
            }
            if (!image) {
                [safeSelf didFinishLoadingImage:nil
                                       forEntry:weakEntry
                                          error:result.error];
            }
            else {
                [safeSelf didFinishLoadingImage:image
                                       forEntry:weakEntry
                                          error:nil];
            }
        }];
        op = cOp;
    }
    else {
        /// Concrete op (Load with resizing image)
        LEXLoadingAndResizingImageOperation *cOp;
        CGFloat limitSide = kLEXImageLimitSide;
        cOp = [[LEXLoadingAndResizingImageOperation alloc] initWithImageURL:URL
                                                             imageLimitSide:limitSide];
        [cOp setQueuedCompletionBlock:^(LEXOperationResult *__unsafe_unretained r) {
            LEXOperationResult *result = (id)r;
            NSLog(@"Get resulgt: %@", result);
            if (result.outputData != nil) {
                [safeSelf didFinishLoadingImage:result.outputData
                                       forEntry:weakEntry
                                          error:nil];
            }
            else {
                [safeSelf didFinishLoadingImage:nil
                                       forEntry:weakEntry
                                          error:result.error];
            }
        }];
        op = cOp;
    }
    op.callbacksDispatchQueue = dispatch_get_main_queue();
    op.dispatchQueue = _complexOpsQueue;
    [self.opQueue addOperation:op];
    return op;
}


- (void)didFinishLoadingImage:(UIImage *)image
                     forEntry:(__weak LEXImagePackEntry *)entry
                        error:(NSError *)error
{
    if (!entry) {
        return;
    }
    
    // Retain entry
    LEXImagePackEntry *anEntry = entry;
    if (image) {
        [anEntry configureWithLoadedImage:image];
    }
    else {
        [anEntry configureWithFailError:error];
    }
    
    NSUInteger idx = [_imageEntries indexOfObjectIdenticalTo:anEntry];
    if (idx != NSNotFound) {
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx
                                                                    inSection:0]]
                              withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.imageEntries.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"ImageCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    /// Image entry at given index path
    LEXImagePackEntry *entry = _imageEntries[indexPath.row];
    if (entry.state == kLEXImagePackEntryStateNotLoaded) {
       [self startImageLoadingOperationWithEntry:entry
                               shouldResizeImage:YES];
        entry.state = kLEXImagePackEntryStateLoading;
    }
    
    cell.imageView.image = entry.image;
    
    NSString *cellText;
    switch (entry.state) {
        case kLEXImagePackEntryStateLoading:
            cellText = @"Loading...";
            break;
        case kLEXImagePackEntryStateNotLoaded:
            // Actually this status should be shown never.
            cellText = @"Not loaded yet";
            break;
        case kLEXImagePackEntryStateFailed:
            cellText = entry.error.localizedDescription;
            break;
        case kLEXImagePackEntryStateLoaded:
            cellText = @"Loaded";
            break;
        default:
            cellText = @"";
            break;
    }
    cell.textLabel.text = cellText;
    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
 {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
