//
//  LEXLoadingAndResizingImageOperation.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 17.07.14.
//
//

#import "LEXComplexOperation.h"

@interface LEXLoadingAndResizingImageOperation : LEXComplexOperation
@property (atomic, readonly) NSURL *URL;
@property (atomic, readonly) CGFloat imageLimitSide;

- (id)initWithImageURL:(NSURL *)URL imageLimitSide:(CGFloat)limitSide;

@end
