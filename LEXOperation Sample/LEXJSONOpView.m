//
//  LEXJSONOpView.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 05.05.14.
//
//

#import "LEXJSONOpView.h"

@implementation LEXJSONOpView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        _contentInsets = UIEdgeInsetsMake(8.0f, 8.0f, 8.0f, 8.0f);
        _labelToButtonMargin = 8.0f;
        
        _ipLabel = [[UILabel alloc] init];
        [self addSubview:_ipLabel];
        
        _requestIPButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        [_requestIPButton setTitle:@"Request IP"
                          forState:UIControlStateNormal];
        [self addSubview:_requestIPButton];
    }
    return self;
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets
{
    if (!UIEdgeInsetsEqualToEdgeInsets(_contentInsets, contentInsets)) {
        _contentInsets = contentInsets;
        [self setNeedsLayout];
    }
}

- (void)setLabelToButtonMargin:(CGFloat)labelToButtonMargin
{
    if (_labelToButtonMargin != labelToButtonMargin) {
        _labelToButtonMargin = labelToButtonMargin;
        [self setNeedsLayout];
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect area = UIEdgeInsetsInsetRect(self.bounds, _contentInsets);
    
    CGFloat width = area.size.width;
    
    CGSize labelSize = [_ipLabel sizeThatFits:CGSizeMake(width, 0.0f)];
    CGSize buttonSize = [_requestIPButton sizeThatFits:CGSizeMake(width, 0.0f)];
    
    CGFloat totalHeight = labelSize.height + buttonSize.height + _labelToButtonMargin;
    
    CGFloat areaContentPosY = area.origin.y + (area.size.height -  totalHeight) / 2;
    
    CGRect labelRect = CGRectMake(area.origin.x + (width - labelSize.width) / 2,
                                  areaContentPosY,
                                  labelSize.width,
                                  labelSize.height);
    _ipLabel.frame = labelRect;
    
    CGRect buttonRect = CGRectMake(area.origin.x +(width - buttonSize.width) / 2,
                                   CGRectGetMaxY(labelRect) + _labelToButtonMargin,
                                   buttonSize.width, buttonSize.height);
    _requestIPButton.frame = buttonRect;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
