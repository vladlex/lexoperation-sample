//
//  LEXImageColoringOperation.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXImageColoringOperation.h"

@implementation LEXImageColoringOperation

- (id)init
{
    return [self initWithImage:nil color:nil];
}

- (id)initWithImage:(UIImage *)image color:(UIColor *)color
{
    self = [super init];
    if (self != nil) {
        _sourceImage = image;
        _color = color;
    }
    return self;
}

- (void)operationDidStart
{
    [super operationDidStart];
    
    UIImage *image = self.sourceImage;
    NSAssert(image, @"Operation should have image to color");

    UIColor *color = self.color;
    if (!color) {
        image = [image copy];
        [self finishWithSuccessAndOutputData:image];
        return;
    }
    
    if (self.isInDebugMode) {
        NSLog(@"%@: begin image context", NSStringFromClass(self.class));
    }
    
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [color setFill];
    CGContextFillRect(context, rect);
    UIImage *coloredImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (self.isInDebugMode) {
        NSLog(@"%@: Image executed from context: %@",
              NSStringFromClass(self.class), coloredImage);
    }
    
    [self finishWithSuccessAndOutputData:coloredImage];
}

@end
