//
//  LEXLoadingAngColoringImageOperation.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXComplexOperation.h"

@interface NSError (LEXLoadingAngColoringImageOperationError)

- (BOOL)hasLEXLoadingAngColoringImageOperationErrorDomain;

+ (instancetype)loadingAndColoringImageWithCode:(NSInteger)code
                           localizedDescription:(NSString *)localizedDescription;

@end

@interface LEXLoadingAngColoringImageOperationResult : LEXOperationResult
@property (nonatomic, strong) UIImage *sourceImage;
// Colored image is actualy 'outputData'
@property (nonatomic, strong) UIImage *coloredImage;

@end

/// LEXLoadingAngColoringImageOperation errors domain. Error domains usually have not 'k' prefix.
extern NSString * const LEXLoadingAngColoringImageOperationErrorDomain;

typedef NS_ENUM(NSInteger, LEXLoadingAngColoringImageOperationError) {
    kLEXLoadingAngColoringImageOperationErrorUnknown = -1,
    kLEXLoadingAngColoringImageOperationErrorFailToExecuteImageFromData,
    kLEXLoadingAngColoringImageOperationErrorFailToColorImage
};

@interface LEXLoadingAngColoringImageOperation : LEXComplexOperation

/** From where image shloud be loaded */
@property (atomic) NSURLRequest *request;
/** Image overlay color */
@property (atomic) UIColor *color;

- (id)initWithURLRequest:(NSURLRequest *)request color:(UIColor *)color;

@end
