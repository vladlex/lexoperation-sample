//
//  LEXHomeViewController.m
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXHomeViewController.h"

#import "LEXLoadingAndColoringImageViewController.h"
#import "LEXRequestIPViewController.h"
#import "LEXImagePackTableViewController.h"

@interface LEXHomeViewController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, readonly) UITableView *tableView;

@end

@implementation LEXHomeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds
                                              style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.autoresizingMask = (UIViewAutoresizingFlexibleHeight |
                                   UIViewAutoresizingFlexibleWidth);
    [self.view addSubview:_tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (_tableView) {
        NSArray *selection = [_tableView indexPathsForSelectedRows];
        if (selection.count > 0) {
            for (NSIndexPath *indexPath in selection) {
                [_tableView deselectRowAtIndexPath:indexPath
                                          animated:animated];
            }
        }
    }
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)presentViewControllerOfClass:(Class)AnVCClass animated:(BOOL)animated
{
    UIViewController *vc = [[AnVCClass alloc] init];
    [self.navigationController pushViewController:vc animated:animated];
    if (self.navigationController.navigationBarHidden) {
        [self.navigationController setNavigationBarHidden:NO animated:YES];
    }
}


#pragma mark  UITableView Delegate & Data Source

- (void)tableView:(UITableView *)tableView
didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self presentViewControllerOfClass:[LEXRequestIPViewController class]
                                      animated:YES];
            break;
        case 1:
            [self presentViewControllerOfClass:[LEXLoadingAndColoringImageViewController class]
                                      animated:YES];
            break;
        case 2:
            [self presentViewControllerOfClass:[LEXImagePackTableViewController class]
                                      animated:YES];
            break;
        default:
            NSLog(@"%@ -> %@. something goes wrong. Check section rows count!",
                  NSStringFromClass(self.class),
                  NSStringFromSelector(_cmd));
            break;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _tableView == tableView ? 1 : 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (section == 0 && tableView == _tableView) ? 3 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"TaskCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
    }
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)configureCell:(UITableViewCell *)cell
          atIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            cell.textLabel.text = @"Request IP";
            cell.detailTextLabel.text = @"Get current device IP";
            break;
        case 1:
            cell.textLabel.text = @"Load and color image";
            cell.detailTextLabel.text = @"Loading image from given URL and coloring it";
            break;
        case 2:
            cell.textLabel.text = @"Load bunch of images";
            cell.detailTextLabel.text = @"Loading image while table view is scrolling";
            break;
        default:
            cell.textLabel.text = @"#ERROR#";
            cell.detailTextLabel.text = @"";
            break;
    }
}

@end
