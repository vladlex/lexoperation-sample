//
//  LEXImageColoringOperation.h
//  LEXOperation Sample
//
//  Created by Alexei Gordeev on 06.05.14.
//
//

#import "LEXOperation.h"

/** This operation coloring given image. Be sure to set image before the operation did start. If image is 'nil' at the moment when operation starts then exception will be raised. */
@interface LEXImageColoringOperation : LEXOperation

/** Image to color */
@property (atomic) UIImage *sourceImage;
/** Image overlay color */
@property (atomic) UIColor *color;

- (id)initWithImage:(UIImage *)image color:(UIColor *)color;

@end
