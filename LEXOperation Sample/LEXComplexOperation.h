//
//  LEXComplexOperation.h
//
//  Created by Aleksei Gordeev on 28.02.14.
//  Copyright (c) 2014 Aleksei Gordeev. All rights reserved.
//

#import "LEXOperation.h"

/** LEXComplexOperation is start point for long complex operation composed by other operations.
 According to Apple docs NSOperation should not get any information for execution from dependent operation and make any suggestion about this information. When operation goes to NSOperationQueue instance it should be finally configured. So the right wat is to create NSOperation instance only when it's actually needed. This class is good place to do it.
 @warning Pay attention to call [super operationDidStart] when override -operationDidStart.
 */
@interface LEXComplexOperation : LEXOperation
@property (atomic, strong) NSOperationQueue *suboperationsQueue;
@property (atomic, strong, readonly) NSOperationQueue *actualSuboperationsQueue;

@property (atomic, strong) dispatch_queue_t suboperationsDispatchQueue;
@property (atomic, strong, readonly) dispatch_queue_t actualSuboperationsDispatchQueue;

@end

@interface LEXComplexOperation (Sublcassing)

/** Normally you have not to override this method. 
 You may use -willAddSuboperation: to configure suboperation before adding. */
- (void)addSuboperation:(LEXOperation *)operation;
/** Override to configure operation. Suboperation actual and callbacks queue are already setup ath this moment. */
- (void)willAddSuboperation:(LEXOperation *)operation;

@end