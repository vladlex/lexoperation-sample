# What's it all about?
This project demonstrates one of the very useful NSOperations subclasses kit which I use in every project. 
The power of NSOperation usually comes when you have a bunch of hardwork code blocks and want to manipulate them in any way.

## Base Classes
Base classes are contained in 'Operation Base' project group. You can easily insert them into your project and inherit them to create you very own target-based operations. All operations classes in this group are abstract.

## Operation Samples
All concrete operations are contained in "Operation samples" project group. Some of them like LEXURLConnectionDataLoadOperation may be useful even without any modifications.